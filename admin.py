import hashlib
import json
import random
import urllib.parse
import urllib.request
import os
import time
import config
import warnings

servers = {
    'local': 'http://localhost:3001/admin/support/devtools',
    'alpha': 'http://admin.nrc-alpha00.plamee.com/admin/support/devtools',
    'nrc_staging': 'http://admin.nrc-staging.plamee.com/admin/support/devtools',
    'nrc_prod': 'http://admin.nrc.plamee.com/admin/support/devtools',
}


class Admin:
    def __init__(self, server='local', secret_key='secret_key', state_path='C:/states/'):
        """
        :param server: server name - urls are in config
        :param state_path: path to save states
        """

        self.url = servers[server]
        # self.url = server
        self.secret_key = secret_key
        self.state_path = state_path
        self.prod_key = 'fwkw3lke4dkd9dii99902sk7f'  # DANGER! PRODUCTION! DO NOT FUCK UP PLEASE

    @staticmethod
    def _form_body(user_id: int, method_id: str) -> dict:
        return {
            'method': 'exec',
            'params': {
                'methodId': method_id,
                'params': {
                    'userId': user_id
                }
            }
        }

    def _crypt_hash(self, body: str) -> str:
        hash_object = hashlib.md5((self.secret_key + body).encode('utf-8'))
        return hash_object.hexdigest()

    def _request(self, body):
        req = urllib.request.Request(self.url)
        req.add_header('Content-Type', 'application/json')
        req.add_header('x-sign-token', self._crypt_hash(body))
        return urllib.request.urlopen(req, body.encode('utf-8'))

    def _get_state(self, user_id: int) -> dict:
        body = self._form_body(user_id, 'editState')
        body['params']['params'].update({'state': {}})
        body = json.dumps(body)

        response = self._request(body)
        params = (json.loads((response.read()).decode('utf-8'))).get('params')
        return params.get('state')

    def _load_state(self, user_id: int, state: dict):
        body = self._form_body(user_id, 'editState')
        body['params']['params'].update({'state': state})
        body = json.dumps(body)
        self._request(body)

    def init(self, uid, authkey):
        body = {
            'platform': "android",
            'version': '1.1.7',
            'userId': uid,
            'authKey': authkey
        }
        self._request(json.dumps(body))

    def register(self, key, sig):
        body = {
            'platform': "android",
            'version': '1.1.7',
            'key': key,
            'sig': sig
        }
        self._request(json.dumps(body))

    def _disable_user(self, user_id: int):
        body = json.dumps(self._form_body(user_id, 'disableUser'))
        self._request(body)

    def drop_user(self, user_id: int):
        self._disable_user(user_id)

    def get_state(self, user_id: int) -> dict:
        return self._get_state(user_id)

    def set_state(self, user_id: int, state: dict):
        self._load_state(user_id, state)

    def save_state(self, state: dict):
        user_id = state['self']['userId']
        json.dump(state, open(os.path.join(self.state_path, '{}.json'.format(str(user_id))), 'w'))

    def save_user_state(self, user_id: int):
        state = self.get_state(user_id)
        self.save_state(state)

    def finish_tournament(self, user_id: int):
        js = self.get_state(user_id)
        try:
            js['self']['tournament']['current']['endTime'] = 0
            # self.set_state(user_id, js)
        except:
            raise KeyError('User {} has no tournament information'.format(str(user_id)))

    def get_map_id(self, user_id: int) -> int:
        return self.get_state(user_id)['map']['id']

    def get_map_owner(self, user_id: int) -> int:
        return self.get_state(user_id)['map']['owner']

    def get_name(self, user_id: int) -> str:
        js = self.get_state(user_id)
        if 'isNameDefined' in js['self']['player']:
            return js['self']['player']['name']
        else:
            warnings.warn("User's {} name is not defined".format(str(user_id)))
            return 'user_{}'.format(user_id)

    def set_name(self, user_id: int, name: str):
        js = self.get_state(user_id)
        js['self']['player']['name'] = name
        js['self']['player']['isNameDefined'] = True
        self.set_state(user_id, js)

    def get_exp(self, user_id: int) -> int:
        return int(self.get_state(user_id)['self']['player']['exp'])

    def set_exp(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['exp'] = amount
        self.set_state(user_id, js)

    @staticmethod
    def calculate_level(exp):
        for key, value in config.levels.items():
            if exp in key:
                return value

    def get_level(self, user_id: int) -> int:
        user_exp = self.get_exp(user_id)
        for key, value in config.levels.items():
            if user_exp in key:
                return value

    def set_level(self, user_id, level):
        self.set_exp(user_id, random.choice(config.exps[level]))

    def get_rating(self, user_id: int) -> int:
        return int(self.get_state(user_id)['self']['player']['rating'])

    def set_rating(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['rating'] = amount
        self.set_state(user_id, js)

    def get_npc_rating(self, user_id: int) -> int:
        return int(self.get_state(user_id)['self']['player']['npcRating'])

    def set_npc_rating(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['npcRating'] = amount
        self.set_state(user_id, js)

    def get_resources(self, user_id: int) -> dict:
        return self.get_state(user_id)['self']['player']['resources']

    def get_resources_limits(self, user_id: int) -> dict:
        return self.get_state(user_id)['self']['resourceLimits']

    def set_resources_limits(self, user_id: int):
        js = self.get_state(user_id)
        for key, value in js['self']['resourceLimits']:
            js['self']['player']['resources'][key] = value
        self.set_state(user_id, js)

    def set_resources_overlimit(self, user_id: int):
        js = self.get_state(user_id)
        for key, value in js['self']['player']['resources']:
            js['self']['player']['resources'][key] = 99999999
        self.set_state(user_id, js)

    def get_product(self, user_id: int) -> int:
        return self.get_resources(user_id)['product']

    def set_product(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['resources']['product'] = amount
        self.set_state(user_id, js)

    def get_bucks(self, user_id: int) -> int:
        return self.get_resources(user_id)['bucks']

    def set_bucks(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['resources']['bucks'] = amount
        self.set_state(user_id, js)

    def get_wood(self, user_id: int) -> int:
        return self.get_resources(user_id)['wood']

    def set_wood(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['resources']['wood'] = amount
        self.set_state(user_id, js)

    def get_real(self, user_id: int) -> int:
        return self.get_resources(user_id)['real']

    def set_real(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['resources']['real'] = amount
        self.set_state(user_id, js)

    def get_iron(self, user_id: int) -> int:
        return self.get_resources(user_id)['iron']

    def set_iron(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['resources']['iron'] = amount
        self.set_state(user_id, js)

    def get_stone(self, user_id: int) -> int:
        return self.get_resources(user_id)['stone']

    def set_stone(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['player']['resources']['stone'] = amount
        self.set_state(user_id, js)

    def get_reputation(self, user_id: int):
        js = self.get_state(user_id)
        if 'reputation' in js['self']:
            return self.get_state(user_id)['self']['reputation']
        else:
            warnings.warn("User {} has no reputation points".format(str(user_id)))
            return {}

    def get_loyalty(self, user_id: int) -> int:
        reputation = self.get_reputation(user_id)
        if 'loyality' in reputation:
            return reputation['loyality']
        else:
            warnings.warn("User {} has no loyalty".format(str(user_id)))
            return 0

    def set_loyalty(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        if 'reputation' in js['self']:
            js['self']['reputation']['loyality'] = amount
        else:
            js['self'].update({'reputation': {'loyality': amount}})
        self.set_state(user_id, js)

    def get_force(self, user_id: int) -> int:
        reputation = self.get_reputation(user_id)
        if 'force' in reputation:
            return reputation['force']
        else:
            warnings.warn("User {} has no force".format(str(user_id)))
            return 0

    def set_force(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        if 'reputation' in js['self']:
            js['self']['reputation']['force'] = amount
        else:
            js['self'].update({'reputation': {'force': amount}})
        self.set_state(user_id, js)

    def get_region(self, user_id: int) -> str:
        return self.get_state(user_id)['self']['player']['region']

    def set_region(self, user_id: int, region: str):
        js = self.get_state(user_id)
        js['self']['player']['region'] = region
        self.set_state(user_id, js)

    def fuse_all_heroes(self, user_id: int, fuse=6):
        js = self.get_state(user_id)

        for hero in js['self']['heroes']:
            js['self']['heroes'][hero]['fusionCounter'] = fuse

        self.set_state(user_id, js)

    def delete_all_heroes(self, user_id: int):
        js = self.get_state(user_id)
        js['self']['heroes'].clear()
        js['self']['lastHeroId'] = 0
        self.set_state(user_id, js)

    def get_session_number(self, user_id: int) -> int:
        return int(self.get_state(user_id)['self']['sessionNumber'])

    def set_session_number(self, user_id, number: int):
        js = self.get_state(user_id)
        js['self']['sessionNumber'] = number
        self.set_state(user_id, js)

    @staticmethod
    def _add_battle_report_user_base(user_state: dict, enemy: dict, cell_x=6, cell_y=1) -> dict:
        report = {
            "type": "npc",
            "targetType": "zone",
            "targetLevel": str(enemy['level']),
            "targetMapId": str(enemy['map_id']),
            "success": True,
            "time": int(time.time() * 1000),
            "aggressorId": str(enemy['id']),
            "aggressorMapId": str(enemy['map_id']),
            "aggressorName": enemy['name'],
            "aggressorLevel": str(enemy['level']),
            "aggressorRating": str(enemy['rating']),
            "squads": [],
            "lostRating": '-2',
            "viewed": False,
            "canRevenge": False,
            "targetCellId": '%s; %s' % (cell_x, cell_y),
        }

        reports = user_state['self']['battleReports']
        reports_amount = len(reports)

        report.update({'id': reports_amount})
        reports.update({str(reports_amount): report})

        return user_state

    @staticmethod
    def _add_battle_report_npc_base(user_state: dict, enemy: dict, cell_x=6, cell_y=1) -> dict:
        report = {
            "type": "npc",
            "targetType": "zone",
            "targetLevel": str(enemy['level']),
            "targetMapId": str(enemy['npcMapId']),
            "success": True,
            "time": int(time.time() * 1000),
            "aggressorId": '',
            "aggressorMapId": '',
            "aggressorName": '',
            "aggressorLevel": 0,
            "aggressorRating": 0,
            "squads": [],
            "lostRating": '-1',
            "viewed": False,
            "canRevenge": False,
            "targetCellId": '%s; %s' % (cell_x, cell_y),
        }

        reports = user_state['self']['battleReports']
        reports_amount = len(reports)

        report.update({'id': reports_amount})
        reports.update({str(reports_amount): report})

        return user_state

    def spawn_user_base(self, user_id: int, enemy_id: int, cell_x=6, cell_y=1):
        enemy_state = self.get_state(enemy_id)

        enemy = {
            'id': enemy_id,
            'level': self.calculate_level(enemy_state['self']['player']['exp']) - 1,
            'map_id': enemy_state['map']['id'],
            'name': enemy_state['self']['player']['name'] if 'name' in enemy_state['self'][
                'player'] else 'user_{}'.format(enemy_id),
            'rating': enemy_state['self']['player']['rating']
        }

        enemy_base = {
            'cellId': '%s; %s' % (cell_x, cell_y),
            "playerId": str(enemy_id),
            'playerMapId': str(enemy['map_id']),
            'time': int(time.time() * 1000),
            'name': enemy['name'],
            'level': str(enemy['level']),
            'battleReward': {
                'consumables': [
                    {
                        'random': random.choice(range(10, 90)),
                        'id': 'plate',
                        'count': 1
                    }
                ],
                'resources': {
                    'iron': random.choice(range(1, 1000)),
                    'wood': random.choice(range(1, 1000)),
                    'bucks': random.choice(range(1, 1000)),
                    'product': random.choice(range(1, 1000)),
                    'stone': random.choice(range(1, 1000))
                },
                'reputation': {
                    'force': 5
                }
            }
            # 'takeoverTime':   int(time.time() * 1000),
            # 'takenCount':     1,
        }

        js = self.get_state(user_id)
        js_map = js['self']['globalMap']['openCells']['{0}; {1}'.format(cell_x, cell_y)]
        js_map.clear()
        js_map.update(enemy_base)

        if js['self']['player']['rating'] > 1:
            js['self']['player']['rating'] -= 2

        self._add_battle_report_user_base(js, enemy, cell_x, cell_y)

        self.set_state(user_id, js)

    def spawn_npc(self, user_id: int, npc_map_id: str, cell_x=6, cell_y=1):
        js = self.get_state(user_id)

        if npc_map_id not in config.npc_bases.keys():
            print("Invalid map id ::: {}".format(npc_map_id))
            return

        npc_base = {
            'cellId': '%s; %s' % (cell_x, cell_y),
            'level': config.npc_bases[npc_map_id]["level"],
            'name': 'enemy_base',
            'npcMapId': npc_map_id
        }

        js_map = js['self']['globalMap']['openCells']['{0}; {1}'.format(cell_x, cell_y)]
        js_map.clear()
        js_map.update(npc_base)

        self._add_battle_report_npc_base(js, npc_base, cell_x, cell_y)

        self.set_state(user_id, js)

    def skip_first_tutorial(self, user_id: int):
        js = self.get_state(user_id)
        js['self']['tutorialStagesDone']['stage1'] = True
        self.set_state(user_id, js)

    def skip_all_tutorials(self, user_id: int):
        js = self.get_state(user_id)
        js['self']['tutorialStagesDone'].update(config.tutorials)
        self.set_state(user_id, js)

    def get_boosters(self, user_id: int) -> dict:
        return self.get_state(user_id)['self']['heroBoostConsumables']

    def set_all_boosters(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        if 'heroBoostConsumables' not in js['self']:
            js['self'].update({'heroBoostConsumables': {}})

        for booster in config.boosters:
            js['self']['heroBoostConsumables'].update({booster: amount})

        self.set_state(user_id, js)

    def get_hq_level(self, user_id: int) -> int:
        return self.get_state(user_id)['self']['levels']['headquarters'] + 1

    def get_tz_offset(self, user_id: int) -> int:
        return self.get_state(user_id)['self']['tzOffset']

    def set_tz_offset(self, user_id: int, offset: int):
        js = self.get_state(user_id)
        # js['self']['tzOffset'] = offset * 1000 * 60 * 60
        js['self']['tzOffset'] = offset
        self.set_state(user_id, js)

    def clean_obstacles(self, user_id: int):
        js = self.get_state(user_id)
        map_items = js['map']['items']
        items_to_clean = []

        for key, value in map_items.items():
            if 'tree' in value['defId']:
                items_to_clean.append(key)
            elif 'debris' in value['defId']:
                items_to_clean.append(key)

        for key in items_to_clean:
            js['map']['items'].pop(key)

        self.set_state(user_id, js)

    def reset_free_chest(self, user_id: int):
        js = self.get_state(user_id)
        js['self']['freeChest']['regular']['time'] = round(time.time() * 1000)
        self.set_state(user_id, js)

    def refresh_tutor_shield(self, user_id: int):
        js = self.get_state(user_id)
        js['self']['buffs']['shield2'] = round(time.time() * 1000 + 86400000)
        self.set_state(user_id, js)

    def set_fyber(self, user_id: int):
        js = self.get_state(user_id)
        js['self']['currentAdContentId'] = 'FyberBoat'
        js['self']['videoOffersViewCounts'] = {}
        js['self']['videoOffersViewCounts']['FyberBoat'] = 0
        js['self']['videoOfferSpawnTimes'] = {}
        js['self']['videoOffersViewCounts']['FyberBoat'] = round(time.time() * 1000 - 86400000)

    def get_zone_count(self, user_id: int) -> int:
        js = self.get_state(user_id)
        return js['self']['globalMap']['capturedZoneCount'] if 'capturedZoneCount' in js['self']['globalMap'] else 0

    def set_zone_count(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        js['self']['globalMap']['capturedZoneCount'] = amount
        self.set_state(user_id, js)

    def set_energy(self, user_id: int, energy: int):
        js = self.get_state(user_id)
        js['self']['player']['energy'] = energy
        self.set_state(user_id, js)

    def enhance_napalm(self, user_id: int):
        js = self.get_state(user_id)
        if 'napalm' in js['self']['actions']:
            js['self']['actions']['napalm']['level'] = 17
        else:
            js['self']['actions'].update({'napalm': {'level': 17, 'stateId': 'idle'}})
        self.set_state(user_id, js)

    def clean_chain(self, user_id: int):
        js = self.get_state(user_id)
        if 'npcChainScores' in js['self']:
            del js['self']['npcChainScores']
        if 'npcChains' in js['self']:
            del js['self']['npcChains']
        if 'npcChainsRewards' in js['self']:
            del js['self']['npcChainsRewards']
        for key, value in js['self']['heroes'].items():
            if 'npcChainId' in value:
                del js['self']['heroes'][key]['npcChainId']
        self.set_state(user_id, js)

    def set_tester_status(self, user_id: int):
        js = self.get_state(user_id)
        if 'isAdmin' in js['self']['player']:
            js['self']['player']['isAdmin'] = True
        else:
            js['self']['player'].update({'isAdmin': True})
        self.set_state(user_id, js)

    def enhance_artillery(self, user_id: int):
        js = self.get_state(user_id)
        if 'artillery' in js['self']['actions']:
            js['self']['actions']['artillery']['level'] = 19
        else:
            js['self']['actions'].update({'artillery': {'level': 19, 'stateId': 'idle'}})
        self.set_state(user_id, js)

    def multiply_boosters(self, user_id: int):
        js = self.get_state(user_id)
        if 'heroBoostConsumables' in js['self']:
            for booster in js['self']['heroBoostConsumables']:
                js['self']['heroBoostConsumables'][booster] = 999
        self.set_state(user_id, js)

    def set_dices(self, user_id: int, amount: int):
        js = self.get_state(user_id)
        if 'heroBoostConsumables' not in js['self']:
            js['self'].update({'heroBoostConsumables': {}})

        js['self']['heroBoostConsumables'].update({'abilityDice': amount})
        self.set_state(user_id, js)

    @staticmethod
    def get_field(state: dict, field: str):
        if field in state["self"]:
            return state["self"][field]
        else:
            return None

    def show_payment_info(self, user_id: int) -> dict:
        js = self.get_state(user_id)
        result = {
            "pendingPayments": self.get_field(js, "pendingPayments"),
            "counterIapPurchased": self.get_field(js, "counterIapPurchased"),
            "moneySpentIapPurchased": self.get_field(js, "moneySpentIapPurchased"),
            "maxIapPurchasedData": self.get_field(js, "maxIapPurchasedData"),
            "maxIapPurchased": self.get_field(js, "maxIapPurchased"),
            "gemSalePeriodIndex": self.get_field(js, "gemSalePeriodIndex")
        }

        for key, value in result.items():
            print("{0}: {1}".format(key, value))

        return result

    def is_sr(self, user_id: int, show_info=False):
        js = self.get_state(user_id)
        if "srPayments" in js['self']:
            result = {}
            if show_info:
                for key, value in js['self']['srPayments'].items():
                    for product, info in value['products'].items():
                        result.update({
                            product: {
                                'reward': info['reward'],
                                'price': info['price']
                            }
                        })

            for key, value in result.items():
                print(key, value)
            return js['self']['srPayments']
        else:
            print("Default products")
            return None

    def is_terms_of_use_accepted(self, user_id: int):
        js = self.get_state(user_id)
        if "isTermsOfUseAccepted" in js['self']:
            print("User {} -> ".format(user_id) + str(js['self']['isTermsOfUseAccepted']))
            return js['self']['isTermsOfUseAccepted']
        else:
            print("No field for ToU in state of user {}".format(user_id))
            return False

    def set_ToU(self, user_id, ac: bool):
        js = self.get_state(user_id)
        if 'isTermsOfUseAccepted' not in js['self']:
            js['self'].update({'isTermsOfUseAccepted': ac})
        else:
            js['self']['isTermsOfUseAccepted'] = ac

        self.set_state(user_id, js)

    def delete_ToU(self, user_id: int):
        js = self.get_state(user_id)
        if 'isTermsOfUseAccepted' in js['self']:
            js['self'].pop('isTermsOfUseAccepted')
            self.set_state(user_id, js)
        else:
            print('User {} has no ToU field'.format(user_id))
